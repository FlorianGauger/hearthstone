package com.myecl.hearthstone.cards;

import java.util.Map;

import com.myecl.hearthstone.enums.Ability;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Rarity;
import com.myecl.hearthstone.enums.Set;

public class Spell extends Card {

	public Spell(int id, String name, String description, int cost,
			Map<Ability, String> abilities, HeroClass heroClass, Rarity rarity,
			Set set) {
		super(id, name, description, cost, abilities, heroClass, rarity, set);
	}

}
