package com.myecl.hearthstone.cards;
import java.util.Map;

import com.myecl.hearthstone.enums.Ability;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Rarity;
import com.myecl.hearthstone.enums.Set;


public class Card {
	private final int id;
	private final String name;
	private final String description;
	private final int cost;
	
	private final HeroClass heroClass;
	private final Rarity rarity;
	private final Set set;
	private final Map<Ability, String> abilities;
	
	
	public Card(int id, String name, String description, int cost,
			Map<Ability, String> abilities, HeroClass heroClass,
			Rarity rarity, Set set) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.cost = cost;
		this.abilities = abilities;
		this.heroClass = heroClass;
		this.rarity = rarity;
		this.set = set;
	}
	
	@Override
	public String toString() {
		return cost + "\t" + heroClass + "\t\t" + name + "\t\t" + description;
	}
	
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public int getCost() {
		return cost;
	}

	public Map<Ability, String> getAbilities() {
		return abilities;
	}

	public HeroClass getHeroClass() {
		return heroClass;
	}

	public Rarity getRarity() {
		return rarity;
	}


	public Set getSet() {
		return set;
	}
}

