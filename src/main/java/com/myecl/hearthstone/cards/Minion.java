package com.myecl.hearthstone.cards;

import java.util.Map;

import com.myecl.hearthstone.enums.Ability;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Race;
import com.myecl.hearthstone.enums.Rarity;
import com.myecl.hearthstone.enums.Set;

public class Minion extends Card {
	private final int attack;
	private final int health;
	private final Race race;
	
	public Minion(int id, String name, String description, int cost,
			Map<Ability, String> abilities,
			HeroClass heroClass, Rarity rarity, Set set,
			int attack, int health, Race race) {
		super(id, name, description, cost, abilities, heroClass, rarity, set);
		this.attack = attack;
		this.health = health;
		this.race = race;
	}

	public int getAttack() {
		return attack;
	}

	public int getHealth() {
		return health;
	}

	public Race getRace() {
		return race;
	}
}
