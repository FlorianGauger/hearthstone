package com.myecl.hearthstone;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.SortedSet;

import com.myecl.hearthstone.cards.Card;
import com.myecl.hearthstone.enums.HeroClass;



public class MainExample {
	

	public static void main(String[] args) {
		
		//All druid cards, no neutrals
		for (Card card: allClass(HeroClass.DRUID, false)) {
			System.out.println(card);
		}
		System.out.println("\n\n");
		
		//All cards with windfury
		for (Card card: descriptionContains("Windfury")) {
			System.out.println(card);
		}
		System.out.println("\n\n");
		
		//Random Deck for Mage (Deck implements Bag<Card>)
		for (Card card: makeRandomDeck(HeroClass.MAGE)) {
			System.out.println(card);
		}
	}
	
	
	private static SortedSet<Card> allClass(HeroClass heroClass, boolean neutral) {
		return CardCollection.INSTANCE.getCards(card -> card.getHeroClass() == heroClass
				|| (neutral && card.getHeroClass() == HeroClass.NEUTRAL));
	}
	
	private static SortedSet<Card> descriptionContains(String value) {
		return CardCollection.INSTANCE.getCards(card -> card.getDescription() != null 
				&& card.getDescription().contains(value));
	}
	
	private static Deck makeRandomDeck(HeroClass heroClass) {
		Deck deck = new Deck(heroClass);
		List<Card> availableCards = new LinkedList<Card>(allClass(heroClass, true));
		
		Random random = new Random();
		while (!deck.isFull()) {
			deck.add(availableCards.get(random.nextInt(availableCards.size())));
		}	
		return deck;
	}
	
}