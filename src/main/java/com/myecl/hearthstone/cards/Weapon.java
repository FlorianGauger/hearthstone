package com.myecl.hearthstone.cards;

import java.util.Map;

import com.myecl.hearthstone.enums.Ability;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Rarity;
import com.myecl.hearthstone.enums.Set;

public class Weapon extends Card{
	private final int attack;
	private final int durability;

	public Weapon(int id, String name, String description, int cost,
			Map<Ability, String> abilities,
			HeroClass heroClass, Rarity rarity, Set set,
			int attack, int durability) {
		super(id, name, description, cost, abilities, heroClass, rarity, set);

		this.attack = attack;
		this.durability = durability;
	}
	
	public int getAttack() {
		return attack;
	}

	public int getDurability() {
		return durability;
	}
}
