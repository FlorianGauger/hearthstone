package com.myecl.hearthstone.enums;

public enum Rarity {
	FREE, COMMON, RARE, EPIC, LEGENDARY
}
