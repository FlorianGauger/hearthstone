package com.myecl.hearthstone.enums;

public enum Set {
	BASIC, EXPERT, REWARD, MISSION, PROMOTION
}
