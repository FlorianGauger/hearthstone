package com.myecl.hearthstone;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.SortedMap;

import org.apache.commons.collections4.Bag;
import org.apache.commons.collections4.SortedBag;
import org.apache.commons.collections4.bag.TreeBag;

import com.myecl.hearthstone.cards.Card;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Rarity;


public class Deck implements Bag<Card>{
	private final int maxNumberFree = 2;
	private final int maxNumberCommon = 2;
	private final int maxNumberRare = 2;
	private final int maxNumberEpic = 2;
	private final int maxNumberLegendary = 1;
	private final int maxSize = 30;
	private SortedBag<Card> cards = new TreeBag<Card>(CardUtil.getCardCostComparator());
	
	private HeroClass heroClass;
	
	public Deck(HeroClass heroClass) {
		this.heroClass = heroClass;
	}
	
	public SortedMap<Integer, Integer> getManaCurve() {
		return CardUtil.getManaCurve(cards);
	}
	
	@Override
	public boolean add(Card card) {
		boolean result = cards.size() < maxSize 
				&& cards.getCount(card) < getMaxNumber(card.getRarity())
				&& (card.getHeroClass() == heroClass 
				|| card.getHeroClass() == HeroClass.NEUTRAL);
		if (result) {
			result = cards.add(card);
		}
	return result;
	}

	

	@Override
	public boolean addAll(Collection<? extends Card> cardsToAdd) {
		boolean result = false;
		for (Card cardToAdd: cardsToAdd) {
			result |= this.add(cardToAdd);
		}
		return result;
	}

	
	@Override
	public boolean add(Card card, int count) {
		boolean result = cards.size() < maxSize 
				&& cards.getCount(card) < getMaxNumber(card.getRarity());
		if (result) {
			for (int i = 0; i < count; i++) {
				this.add(card);
			}
		}
		return false;
	}

	@Override
	public int getCount(Object arg0) {
		return cards.getCount(arg0);
	}

	@Override
	public boolean remove(Object arg0, int arg1) {
		return cards.remove(arg0);
	}

	@Override
	public Set<Card> uniqueSet() {
		return cards.uniqueSet();
	}
	
	@Override
	public int size() {
		return cards.size();
	}

	@Override
	public boolean isEmpty() {
		return cards.isEmpty();
	}
	
	public boolean isFull() {
		return cards.size() >= maxSize;
	}

	@Override
	public boolean contains(Object o) {
		return cards.contains(o);
	}

	@Override
	public Iterator<Card> iterator() {
		return cards.iterator();
	}

	@Override
	public Object[] toArray() {
		return cards.toArray();
	}

	@Override
	public <T> T[] toArray(T[] a) {
		return cards.toArray(a);
	}
	
	@Override
	public boolean retainAll(Collection<?> c) {
		return cards.retainAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		return cards.retainAll(c);
	}

	@Override
	public void clear() {
		cards.clear();
	}
	
	@Override
	public boolean remove(Object o) {
		return cards.remove(o);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		return cards.containsAll(c);
	}
	
	private int getMaxNumber(Rarity rarity) { 
		int number = -1;
		switch(rarity) {
			case FREE: number = maxNumberFree; break;
			case COMMON: number = maxNumberCommon; break;
			case RARE: number = maxNumberRare; break;
			case EPIC: number = maxNumberEpic; break;
			case LEGENDARY: number = maxNumberLegendary; break;
		}
		return number;
	}
}
