package com.myecl.hearthstone.enums;

public enum HeroClass {
	NEUTRAL, WARRIOR, SHAMAN, ROGUE, PALADIN, 
	HUNTER, DRUID, WARLOCK, MAGE, PRIEST
}
