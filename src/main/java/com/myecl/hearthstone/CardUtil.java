package com.myecl.hearthstone;
import java.util.Collection;
import java.util.Comparator;
import java.util.SortedMap;
import java.util.TreeMap;

import com.myecl.hearthstone.cards.Card;


public class CardUtil {
	
	private static Comparator<Card> cardCostComparator = new Comparator<Card>() {
		@Override
		public int compare(Card card1, Card card2) {
			int order = card1.getCost() - card2.getCost();
			if (order == 0) {
				order = card1.getName().compareTo(card2.getName());
			}
			return order;
		}
	};
	
	private static Comparator<Card> cardNameComparator = new Comparator<Card>() {
		@Override
		public int compare(Card card1, Card card2) {
			return card1.getName().compareTo(card2.getName());
		}
	};
	
	private CardUtil() {
	}
	

	
	public static SortedMap<Integer, Integer> getManaCurve(Collection<Card> cards) {
		SortedMap<Integer, Integer> manaCurve = new TreeMap<Integer, Integer>();
		for (Card card: cards) {
			if (!manaCurve.containsKey(card.getCost())) {
				manaCurve.put(card.getCost(), 1);
			} else {
				manaCurve.put(card.getCost(), manaCurve.get(card.getCost()) + 1);
			}
		}
		return manaCurve;
	}
	
	public static Comparator<Card> getCardCostComparator() {
		return cardCostComparator;
	}
	
	public static Comparator<Card> getCardNameComparator() {
		return cardNameComparator;
	}	
}
















