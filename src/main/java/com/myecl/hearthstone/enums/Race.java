package com.myecl.hearthstone.enums;

public enum Race {
	NEUTRAL, MURLOC, DEMON, BEAST, TOTEM, PIRATE, DRAGON
}
