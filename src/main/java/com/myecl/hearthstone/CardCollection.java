package com.myecl.hearthstone;

import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Predicate;

import com.google.gson.stream.JsonReader;
import com.myecl.hearthstone.cards.Card;
import com.myecl.hearthstone.cards.Minion;
import com.myecl.hearthstone.cards.Spell;
import com.myecl.hearthstone.cards.Weapon;
import com.myecl.hearthstone.enums.HeroClass;
import com.myecl.hearthstone.enums.Race;
import com.myecl.hearthstone.enums.Rarity;
import com.myecl.hearthstone.enums.Set;


public enum CardCollection {
	INSTANCE;
	
	private final String cardFilePath = "src/main/config/cards.json";
	private final SortedSet<Card> cardsSortedCost = new TreeSet<Card>(CardUtil.getCardCostComparator());
	
	
	
	CardCollection() {
		parseFormatCardFile();
		initializeCards();
	}
	
	/**
	 * Returns all cards in the Collection in the order specified
	 * by the <code>cardComparator</code>. If the <code>cardComparator</code> is <code>null</code>
	 * the standard hearthstone ordering is used.
	 * 
	 * @param cardComparator the comparator specifying the order of the cards
	 * @return all cards in the specified order
	 */
	public SortedSet<Card> getAllCards(Comparator<Card> cardComparator) {
		if (cardComparator == null) {
			cardComparator = CardUtil.getCardCostComparator();
		}
		SortedSet<Card> allCards = new TreeSet<Card>(cardComparator);
		for (Card card: cardsSortedCost) {
			allCards.add(card);
		}
		return allCards;
	}
	
	/**
	 * Returns all cards that match the given <code>predicate</code>.
	 * Not all cards contain values for all fields!
	 * 
	 * @param predicate the predicate used to test the cards
	 * @return all cards that match the predicate
	 * @throws NullPointerException thrown when the predicate uses a null value without checking it
	 */
	public SortedSet<Card> getCards(Predicate<Card> predicate) throws NullPointerException {
		SortedSet<Card> matchingCards = new TreeSet<Card>(CardUtil.getCardCostComparator());
		for (Card card: cardsSortedCost) {
			if (predicate.test(card)) {
				matchingCards.add(card);
			}
		}
		return matchingCards;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	private void parseFormatCardFile() {
		Map<String, String> replaceMap = new HashMap<String, String>();
		
		//Rarity 
		replaceMap.put("\"quality\": 0", "\"rarity\": \"Free\"");
		replaceMap.put("\"quality\": 1", "\"rarity\": \"Common\"");
		replaceMap.put("\"quality\": 3", "\"rarity\": \"Rare\"");
		replaceMap.put("\"quality\": 4", "\"rarity\": \"Epic\"");
		replaceMap.put("\"quality\": 5", "\"rarity\": \"Legendary\"");
		
		//Set
		replaceMap.put("\"set\": 2", "\"set\": \"Basic\"");
		replaceMap.put("\"set\": 3", "\"set\": \"Expert\"");
		replaceMap.put("\"set\": 4", "\"set\": \"Reward\"");
		replaceMap.put("\"set\": 5", "\"set\": \"Mission\"");
		replaceMap.put("\"set\": 11", "\"set\": \"Promotion\"");
		
		//Type
		replaceMap.put("\"type\": 4", "\"type\": \"Minion\"");
		replaceMap.put("\"type\": 5", "\"type\": \"Spell\"");
		replaceMap.put("\"type\": 7", "\"type\": \"Weapon\"");
		
		//Classes
		replaceMap.put("\"classs\": null", "\"heroClass\": \"Neutral\"");
		replaceMap.put("\"classs\": 1", "\"heroClass\": \"Warrior\"");
		replaceMap.put("\"classs\": 2", "\"heroClass\": \"Paladin\"");
		replaceMap.put("\"classs\": 3", "\"heroClass\": \"Hunter\"");
		replaceMap.put("\"classs\": 4", "\"heroClass\": \"Rogue\"");
		replaceMap.put("\"classs\": 5", "\"heroClass\": \"Priest\"");
		replaceMap.put("\"classs\": 7", "\"heroClass\": \"Shaman\"");
		replaceMap.put("\"classs\": 8", "\"heroClass\": \"Mage\"");
		replaceMap.put("\"classs\": 9", "\"heroClass\": \"Warlock\"");
		replaceMap.put("\"classs\": 11", "\"heroClass\": \"Druid\"");
		
		//Race
		replaceMap.put("\"race\": null", "\"race\": \"Neutral\"");
		replaceMap.put("\"race\": 14", "\"race\": \"Murloc\"");
		replaceMap.put("\"race\": 15", "\"race\": \"Demon\"");
		replaceMap.put("\"race\": 20", "\"race\": \"Beast\"");
		replaceMap.put("\"race\": 21", "\"race\": \"Totem\"");
		replaceMap.put("\"race\": 23", "\"race\": \"Pirate\"");
		replaceMap.put("\"race\": 24", "\"race\": \"Dragon\"");
		
		
		Path path = Paths.get(cardFilePath);
		try {
			String content = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
			for (Map.Entry<String, String> entry: replaceMap.entrySet())
			content = content.replaceAll(entry.getKey(), entry.getValue());
			Files.write(path, content.getBytes(StandardCharsets.UTF_8));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void initializeCards() {
		try (JsonReader reader = new JsonReader(new FileReader(cardFilePath))) {	
			reader.beginArray();
			while (reader.hasNext()) {
				cardsSortedCost.add(readCard(reader));
			}
			reader.endArray();	
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private static Card readCard(JsonReader reader) {
		Card card = null; 
		int id = -1;
		String cardName = "";
		String description = "";
		int cost = -1;
		
		HeroClass heroClass = null;
		Set set = null;
		Rarity rarity = null;
		Race race = null;
		
		String type = "";
		int attack = -1;
		int health = -1;
		int durability = -1;
		
		try {
			reader.beginObject();
		    	while (reader.hasNext()) {
		    		String name = reader.nextName();
				    if (name.equals("id")) {
				    	id = reader.nextInt();
				    } else if (name.equals("name")) {
				    	cardName = reader.nextString();
				    } else if (name.equals("description")) {
				    	description = reader.nextString();
				    } else if (name.equals("cost")) {
				    	cost = reader.nextInt();
				    	
				    //Enums	
				    } else if (name.equals("heroClass")) {
				    	heroClass = HeroClass.valueOf(reader.nextString().toUpperCase());
				    } else if (name.equals("set")) {
				    	set = Set.valueOf(reader.nextString().toUpperCase());
				    } else if (name.equals("rarity")) {
				    	rarity = Rarity.valueOf(reader.nextString().toUpperCase());
				    } else if (name.equals("race")) {
				    	race = Race.valueOf(reader.nextString().toUpperCase());
				    	
				    //Type spceific
				    } else if (name.equals("type")) {
				    	type = reader.nextString();
				    } else if (name.equals("attack")) {
				    	attack = reader.nextInt();
				    } else if (name.equals("health")) {
				    	health = reader.nextInt();
				    } else if (name.equals("durability")) {
				    	durability = reader.nextInt();
				    	
				    } else {
		    	   		reader.skipValue();
		       		}
		     	}
		    	if (heroClass == null) {
		    		heroClass = HeroClass.NEUTRAL;
		    	}
		     reader.endObject();
		     switch(type) {
		     	case "Minion": card = new Minion(id, cardName, description, cost, null, heroClass, rarity, set, attack, health, race); break;
		     	case "Spell": card = new Spell(id, cardName, description, cost, null, heroClass, rarity, set); break;
		     	case "Weapon": card = new Weapon(id, cardName, description, cost, null, heroClass, rarity, set, attack, durability); break;
		     }
	     } catch (IOException e) {
	    	 e.printStackTrace();
	     }
	     return card;
	 }
}